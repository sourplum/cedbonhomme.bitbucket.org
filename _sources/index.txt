.. doc_internship documentation master file, created by
   sphinx-quickstart on Fri Jun  5 11:54:08 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation
=============

.. raw:: html

    <h4>Internship Cedric Bonhomme</h4>
    <p>Here you will find informations on the internship.</p>

This is but an **Introduction**, or more what I've been doing **before** the `Main Project`_. For Informations/Documentation about the `Main Project`_ please click on the button bellow: 

.. _`Main Project`: http://twinstrangers.github.io/

.. raw:: html

    <a href="http://twinstrangers.github.io/" class="btn btn-primary btn-lg" role="button" target="_blank" >Go to TwinStrangers Documentation</a>

.. rubric:: Content

.. toctree::
   :numbered:
   :maxdepth: 2
   
   en/ch01/index
   en/ch02/index
   en/ch03/index
   en/ch04/index

