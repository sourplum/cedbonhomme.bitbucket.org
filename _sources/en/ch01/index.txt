========
Overview
========

What this is about
==================

.. topic:: Abstract

    TwinStranger is a new show on Youtube where the 3 participants had to find their TwinStranger or lookalike within a month. The success of it on social medias and in the hope to launch a show on the main Tv provider in Ireland. The company behind it decided to launch an App for it to maintain the community.
    
    The application TwinStrangers is a mobile application to promote the show. Their is a section to have the latest updates about progress of each participant of the show and a page for the community to talk about it (twitter and/or facebook). However the main goal of this application is more than that.
    It is for the user of the app to also find their TwinStranger or doppelganger!


.. figure:: assets/newspaper.png
    :width: 50%
    :align: center
    :alt: TwinStranger made a worlwide Buzz!
    :figclass: align-center
    
    TwinStranger made a worlwide Buzz!
