==============
Change of Plan
==============

Face recognition API
====================

As stated in the previous chapter, we need to setup a Server/Database with all the users' photos and facial recognition algorithms on it.
We could do that or ...

Face++
------

The Chinese giant, used by local search engines. Doing better than the previous face.com (closed since Facebook bough it).

How to use it:

.. image:: assets/workflow.png

#. Detect human faces.
#. Crop and align.
#. Train a face (Fisherfaces or Eigenfaces)

the API:

    * **Detect Face**: Invoke ``/detection/detect`` to extract face location and attributes from input Image, and generate the face_id of target Faces.
    * **Create Person**: Invoke ``/person/create`` to create a Person, and use ``/person/add_face`` to add Faces into this Person
    * **Create Group**: Invoke ``/group/create`` to create a new Group, use ``/group/add_person`` to add candidate Persons.
    * **Train Model**: Invoke ``/recognition/train`` to train the models for recognition and search use.
    * **Recognize Face**: Invoke ``/recognition/recognize`` for recognizing queried Face within the candidate Group.

See `Demos API Face++`_

.. _`Demos API Face++`: http://www.faceplusplus.com/demo-detect/

Lambda labs
-----------

Small team settled at Seatle. Won prices for the accuracy of their algorithms.

See `Demos API Lambdal`_

.. _`Demos API Lambdal`: https://lambdal.com/face-recognition-api

Filters by Facial Features
==========================

Seeing that the facial recognition will lead us nowhere near what we hoped. We came with the idea of filter by facial features:

    * The user will describe themselves and will be able to see others sharing the same facial features as she/he does.

New Project: TwinStrangers Website
==================================

The TwinStrangers Website has been my main project over the internship.
Here is a `link to the Main Project`_ that I'm proud to present to you:

.. raw:: html

    <a href="http://twinstrangers.github.io/" class="btn btn-primary btn-lg" role="button" target="_blank" >Go to TwinStrangers Documentation</a>

.. _`link to the Main Project`: http://twinstrangers.github.io/